// Javascript Array 

// Array Basic Structure
/* Syntax:
	let/const arrayName = [elementA, elementB, elementC...];	
*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of mixed elements in an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, true];

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass',
];

// Accesing elements in an array
console.log(grades);
console.log(computerBrands[2], computerBrands[7]);
console.log(computerBrands[4]);


// Reassign array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'hello world';
console.log(myTasks);

// Array Methods

// Mutator Methods
// - are functions that 'mutate'or change an array after they're created. 

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push ()
// - add an element in the end of an array and returns the array's length
/* Syntax:
	arrayName.push();
*/

console.log('Current array:');
console.log(fruits);
let fruitslength = fruits.push('Mango'); 
console.log(fruitslength);
console.log('Mutated array from push method:');
console.log(fruits);

//  pop()
// - removes the last element in an array and returns the removed element
/* Syntax:
	arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// unshift()
/*
	- adds one or more elements at the beginning of an array
	- Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB')
*/

fruits.unshift('lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// shift 
/*
	- removes an element at the beginning of an array and returns the removed element
	- Syntax: 
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()
/*
	- simultaneously removes elements from a specified index number and adds elements
	- Syntax:
		array.Name.splice(startingIndex, deleteCount, elementTBeAdded)
*/

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice:');
console.log(fruits);

// Banana, Apple, Orange, Kiwi, Dragon Fruit

// sort()
/*
	- rearranges the array elements in alphanumeric order
	- Syntax: 
		arrayName.sort();

*/

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

// reverse()
/*
	- reverses the order of array elements
	- Syntax: 
		arrayName.reverse();
*/

fruits.reverse();
console.log('Mutated array from reverse method:');
console.log(fruits);

// Non-mutator methods
// - these are functions that do not modify or change an array after they're created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH','PH', 'FR', 'DE'];

// indexOf()
/*
	- returns an index number of the first matching element found in an array
	- if no match was found, the result will be -1. 
	- Syntax: 
		arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method:' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method:' + invalidCountry);

// lastIndexOf
/*
	- returns the index number of the last matching element found in an array
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex)
*/
// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// Slice()
/*
	- portion/slices elements from an array and returns a new array
	- Syntax:
		arrayName.slice(staringIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(sliceArrayA);

// Slicing off elements to another index
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method:');
console.log(slicedArrayB);

// toString()
/*
	- returns an array as a string separated by commas
	- Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString Method:');
console.log(stringArray);

// concat()
/*
	- combines two arrays and returns the combined result
	- Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink htmk', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = tasksArrayA.concat('smell express', 'thros react');
console.log('Result from concat method: ');
console.log(combinedTasks);

// join()
/*
	- returns and array as a string separated by specified separator string
	- Syntax: 
		arrayName.join('separatorString');
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join('-'));
console.log(users.join(','));

// Iteration methods
// - are loops designed to perform repetitive tasks on arrays 

// forEach()
/*
	- similar to a for loop that iterates on each array element
	- Syntax:
		arrayName.forEach(function(indivElement){
			statement;
		})
*/

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log('Result of filtered tasks:');
console.log(filteredTasks);

// map()
/*
	- iterates on each element and returns a new array with different values depending on the result of the function's operation.
	- Syntax:
		let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
})

console.log('Result of map method:');
console.log(numberMap);

// every()
/*
	- Checks if all elements in an array meet the given condition
	- Syntax:
		let/const resultArray = arrayName.every(function(indivElement){
			return condition;
		})
*/

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log('Result of every method:');
console.log(allValid);

// some()
/*
	- checks if at least one element in the array meets the given condition
	- Syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log('Result of some method:');
console.log(someValid);

// filter()
/*
	- returns a new array that contains elements which meets the given condition
	- Syntax: 
		let/const resultArray = arrayName.filter(function(indivElement){
		return condition;
		})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log('Result of filter method:');
console.log(filterValid);

// No elements found
let nothingFound = numbers.filter(function(number){
	return (number === 0);
})
console.log('Result of filter method:');
console.log(nothingFound);

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
})
console.log('Result of filter method:');
console.log(filteredNumbers);

let products = ['Mouse', 'Keyboard', 'laptop', 'Monitor'];

// include method

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log('Result of includes method:');
console.log(filteredProducts);

// reduce()
/*
	- evaluates element from left to right and returns/reduces the array into a single value
	- Syntax: 
		let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression/operation;
		})
*/

let iteration = 0;

// numbers = [1, 2, 3, 4, 5]
let reducedArray = numbers.reduce(function(x, y){
	console.warn('cureent iteration:' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// operation t oreduce the array into a single value
	return x + y;
}) 
console.log('Result of reduced method: ' + reducedArray);

// reducing string array

let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y){
	return x + ' ' + y;
})

console.log('Result of reduce method: ' + reducedJoin);